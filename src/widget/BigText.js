function BigText(props) {
  if (props.text) {
    return <h1>{props.text}</h1>;
  } else {
    return <h1>No Text Found</h1>;
  }
}

export default BigText;

// https://gitlab.com/sommai.k/react-my-app

// git clone https://gitlab.com/sommai.k/react-my-app
