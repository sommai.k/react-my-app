import React from "react";
import "./HelloWorld.css"
const name = "Hello World";

function sayName(name) {
    if(name) return <b>"This is your" {name}</b>
    else return <b>no name found</b>
}

const element = <h1 className="red-tag">{ sayName(name) }</h1>;
const yahooLink = "http://www.yahoo.com";

const element2 = React.createElement('h1',{
    className: "red-tag"
}, "This is a element 2");

function HelloWorld() {
    return (
        <div>
            <h1>{ sayName(name) }</h1>
            <h1>This is my first page</h1>
            {element}
            <h2>{ sayName() }</h2>
            <a href="www.google.com" target="_blank">Google</a>
            <a href={yahooLink} target="_blank">Yahoo!</a>
            {element2}
        </div>
    );
}

export default HelloWorld;
