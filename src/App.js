import "./App.css";
import HelloWorld from "./widget/HelloWorld";
import BigText from "./widget/BigText";

function App() {
  return (
    <div>
      <BigText text="this is a big text" value="" />
      <BigText />
      <HelloWorld />
    </div>
  );
}

export default App;
